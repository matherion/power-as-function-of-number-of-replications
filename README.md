# Power as a function of number of replications

Project van Stefan en mij: je kunt bepalen hoeveel power je studie moet hebben
door vast te stellen hoeveel replicaties je wil doen / bereid bent om te doen
voordat je conclusies gaat trekken.

Bijvoorbeeld:

```
> .2 ^ 10
[1] 1.024e-07
> .05 ^ 5
[1] 3.125e-07
```

Dus, bij 20% power heb je 10 studies nodig om de kans dat je iets mist heeeeeel
klein te maken; en bij 95% power heb je er maar 5 nodig.

